#!/usr/bin/env python

import roslib;
import sys
import rospy

import tf
import math
import std_srvs.srv
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Twist, Point, Quaternion
from math import radians, sqrt, pow, pi
from angles import normalize_angle, shortest_angular_distance


class LoopSemi:

    def __init__(self):

        self.started = False
        linear_speed = 0.1
        kp = 4
        rounds = 32768
        file = '/workspace/lib/ros_workspace/src/roslime_examples/nodes/e0.txt'
        #file = 'e0.txt'

        def start_track(request):
            self.started = request.data
            return std_srvs.srv.SetBoolResponse(
                success=True,
                message='running' if self.started else 'stopped'
            )

        start_track = rospy.Service('/start_track', std_srvs.srv.SetBool, start_track)

        waypoints = []
        with open(file, 'r') as filehandle:
            filecontents = filehandle.readlines()
            for line in filecontents:
                current_point =  line.strip('\n').split(' ')
                waypoints.append(Point(float(current_point[0]), float(current_point[1]), 0))

            self.base_frame = 'base_footprint'
            self.odom_frame = 'odom'
            self.angular_tolerance = math.pi
            self.distance_tolerance = 0.1
            rospy.init_node('loop_semi', anonymous=False)
            rospy.on_shutdown(self.shutdown)

            self.cmd_vel = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
            rate = 20
            r = rospy.Rate(rate)

            self.tf_listener = tf.TransformListener()
            rospy.sleep(1)

            try:
                self.tf_listener.waitForTransform(self.odom_frame, self.base_frame, rospy.Time(), rospy.Duration(1.0))
            except (tf.Exception, tf.ConnectivityException, tf.LookupException):
                rospy.loginfo('could not transform between %s to %s', self.base_frame, self_odom_frame)
                rospy.signal_shutdown('tf exception waiting for transform')

            wp_len = len(waypoints)

        if not self.started:
            rospy.loginfo('waiting for start signal')

        while not rospy.is_shutdown():
            if self.started:
                break
            r.sleep()

        for wp_index in range(wp_len * rounds):

            if rospy.is_shutdown():
                break

            position = Point()
            (position, rotation) = self.get_position()
            (roll, pitch, th) = euler_from_quaternion(rotation)

            move_cmd = Twist()

            dx = waypoints[wp_index % wp_len].x - position.x
            dy = waypoints[wp_index % wp_len].y - position.y
            distance = sqrt(pow(dx, 2) + pow(dy, 2))
            goal_th = math.atan2(dy, dx)

            rospy.loginfo('wp_index: %s, initial distance: %s', wp_index % wp_len, distance)

            while math.fabs(distance) > self.distance_tolerance and not rospy.is_shutdown():

                (position, rotation) = self.get_position()
                (roll, pitch,th) = euler_from_quaternion(rotation)
                dx = waypoints[wp_index % wp_len].x - position.x
                dy = waypoints[wp_index % wp_len].y - position.y
                distance = sqrt(pow(dx, 2) + pow(dy, 2))
                goal_th = math.atan2(dy, dx)

                angular_error = shortest_angular_distance(goal_th, th)

                # rospy.loginfo('distance: %s, angular_error: %s', distance, angular_error)

                if math.fabs(angular_error) > self.angular_tolerance:
                    move_cmd.linear.x = 0.0
                    move_cmd.angular.z = -4 * angular_error
                else:
                    move_cmd.linear.x = linear_speed
                    move_cmd.angular.z = -kp * angular_error

                self.cmd_vel.publish(move_cmd)

                while not self.started and not rospy.is_shutdown():
                    self.cmd_vel.publish(Twist())
                    r.sleep()

                r.sleep()

            while not self.started and not rospy.is_shutdown():
                self.cmd_vel.publish(Twist())
                r.sleep()

        # stop robot
        self.cmd_vel.publish(Twist())
        rospy.loginfo('end program')

    def get_position(self):
        try:
            (trans, rot) = self.tf_listener.lookupTransform(self.odom_frame, self.base_frame, rospy.Time(0))
        except (tf.Exception, tf.ConnectivityException, tf.LookupException):
            rospy.loginfo('tf exception getting odometer data')
            return
        return Point(*trans), rot

    def shutdown(self):
        rospy.loginfo('shutdown program')
        self.cmd_vel.publish(Twist())
        rospy.sleep(1)


if __name__ == '__main__':
    try:
        LoopSemi()
    except Exception as e:
        print(e)

