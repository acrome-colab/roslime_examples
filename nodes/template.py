#!/usr/bin/env python

import sys
import rospy


class Template:

    def __init__(self):
        rospy.init_node('template', anonymous=False)
        rospy.loginfo('start program')
        rospy.loginfo('end program')

    def shutdown(self):
        rospy.loginfo('shutdown program')
        rospy.sleep(1)


if __name__ == '__main__':
    try:
        Template()
    except Exception as e:
        print(e)
