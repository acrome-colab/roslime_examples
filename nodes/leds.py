#!/usr/bin/env python

import roslib
import sys
import rospy
from roslime.msg import LedColor


class Leds:

    def __init__(self):
        rospy.init_node('leds', anonymous=False)
        r = rospy.Rate(8)
        leds_pub = rospy.Publisher('/leds', LedColor, queue_size=5)

        fl = LedColor()
        top = LedColor()
        fr = LedColor()

        fl.r = 128
        top.r = 128
        fr.r = 128

        fl.index = 0
        top.index = 1
        fr.index = 2

        count = 0
        r.sleep()

        while count < 4 and not rospy.is_shutdown():
            fl.r = 128
            leds_pub.publish(fl)
            r.sleep()
            fl.r = 0
            leds_pub.publish(fl)

            top.r = 128
            leds_pub.publish(top)
            r.sleep()
            top.r = 0
            leds_pub.publish(top)

            fr.r = 128
            leds_pub.publish(fr)
            r.sleep()
            fr.r = 0
            leds_pub.publish(fr)

            count = count + 1

        fl.r = 0
        top.r = 0
        fr.r = 0
        leds_pub.publish(fl)
        leds_pub.publish(top)
        leds_pub.publish(fr)

        print('end program')

    def shutdown(self):
        rospy.loginfo('shutdown program')
        rospy.sleep(1)


if __name__ == '__main__':
    try:
        Leds()
    except Exception as e:
        print(e)
