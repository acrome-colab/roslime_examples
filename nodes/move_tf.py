#!/usr/bin/env python

import roslib;
import sys
import rospy

import tf
import math
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Twist, Point, Quaternion
from math import radians, sqrt, pow, pi
from angles import normalize_angle, shortest_angular_distance


class MoveTf:

    def __init__(self):

        if len(sys.argv) != 5:
            print('Usage: move_tf.py speed kp x y')
            sys.exit()

        goal = Point()
        goal.x = float(sys.argv[3])
        goal.y = float(sys.argv[4])
        linear_speed = float(sys.argv[1])
        kp = float(sys.argv[2])
        self.base_frame = 'base_footprint'
        self.odom_frame = 'odom'
        self.angular_tolerance = math.pi / 2
        self.distance_tolerance = 0.1
        rospy.init_node('move_tf', anonymous=False)
        rospy.on_shutdown(self.shutdown)

        self.cmd_vel = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
        rate = 20
        r = rospy.Rate(rate)

        self.tf_listener = tf.TransformListener()
        rospy.sleep(1)

        try:
            self.tf_listener.waitForTransform(self.odom_frame, self.base_frame, rospy.Time(), rospy.Duration(1.0))
        except (tf.Exception, tf.ConnectivityException, tf.LookupException):
            rospy.loginfo('could not transform between %s to %s', self.base_frame, self.odom_frame)
            rospy.signal_shutdown('tf exception waiting for transform')

        position = Point()
        (position, rotation) = self.get_position()
        (roll, pitch, th) = euler_from_quaternion(rotation)

        move_cmd = Twist()

        dx = goal.x - position.x
        dy = goal.y - position.y
        distance = sqrt(pow(dx, 2) + pow(dy, 2))
        goal_th = math.atan2(dy, dx)

        rospy.loginfo('initial distance: %s', distance);

        while abs(distance) > self.distance_tolerance and not rospy.is_shutdown():
            (position, rotation) = self.get_position()
            (roll, pitch, th) = euler_from_quaternion(rotation)
            dx = goal.x - position.x
            dy = goal.y - position.y
            distance = sqrt(pow(dx, 2) + pow(dy, 2))
            goal_th = math.atan2(dy, dx)

            angular_error = shortest_angular_distance(goal_th, th)

            rospy.loginfo('distance: %s, angular_error: %s', distance, angular_error)

            if math.fabs(angular_error) > self.angular_tolerance:
                move_cmd.linear.x = 0.0
                move_cmd.angular.z = -kp * angular_error
            else:
                move_cmd.linear.x = linear_speed
                move_cmd.angular.z = -kp * angular_error
            self.cmd_vel.publish(move_cmd)
            r.sleep()

        # stop robot
        self.cmd_vel.publish(Twist())

        rospy.loginfo('end program')

    def get_position(self):
        try:
            (trans, rot) = self.tf_listener.lookupTransform(self.odom_frame, self.base_frame, rospy.Time(0))
        except (tf.Exception, tf.ConnectivityException, tf.LookupException):
            rospy.loginfo('tf exception getting odometer data')
            return
        return Point(*trans), rot

    def shutdown(self):
        rospy.loginfo('shutdown program')
        self.cmd_vel.publish(Twist())
        rospy.sleep(1)


if __name__ == '__main__':
    try:
        MoveTf()
    except Exception as e:
        print(e)
