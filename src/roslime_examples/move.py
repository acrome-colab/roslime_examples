from __future__ import division
from math import pi


class MotorCommand:
    def __init__(self):
        self.left = 0
        self.right = 0


class Controller:

    def __init__(self):
        self.maxMotorSpeed = 100  # RPM

    def getSpeeds(self, linearSpeed, angularSpeed):
        speeds = MotorCommand()

        return speeds

    def setWheelSeparation(self, separation):
        self.wheelSeparation = separation

    def setMaxMotorSpeed(self, limit):
        self.maxMotorSpeed = limit

    def setTicksPerMeter(self, ticks):
        self.ticksPerMeter = ticks
